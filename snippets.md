# Start docker on boot  
	systemctl enable docker

# Run a container from image
	docker run -it --name="gentoo" --hostname="gentoo.riky.net" wking/gentoo-en-us bash


# Enter a container 
	docker attach citadel  (Exit with ^P ^Q to keep it running) 
	docker exec -it gogs bash

# Commit a image  
	docker commit citadel robotamer/citadel:v2

# Run a gogs repo container  
	docker run -d --name="gogs" --hostname="gogs" -p 22:22 -p 3000:3000 -v /var/gogs:/data gogs

# Run & enter container  
	docker run -t -i debian:testing /bin/bash

# Run a citadel mail container  
	docker run -d --name="citadel" --hostname="mail" 03493f0ea66d
	docker run -it --name="citadel" --hostname="mail.riky.net" acc12703ecf1 bash

# Build an image from a dockerfile  
	docker build -t robotamer/debian debian
	docker build -t robotamer/citadel citadel

# Run a data folder  
	docker run -d --name="data" --hostname="data" e8069f06f884



# execute a bash to get in
	docker exec -it citadel bash


# citadel EXIT WITH ^p ^q
	docker attach mail


citadel create
```
	docker run -it --name="citadel" --hostname="mail.riky.net" acc12703ecf1 bash
	dockersetlocalhosts v
	doctrl start
	^P ^Q (exit)
```
citadel start up
```
	docker start citadel
	./container hosts
	docker attach citadel
	doctrl start
	^P ^Q (exit)
```