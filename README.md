

docker bash scripts
=================== 

### container

	./container list 	=> Get Container list
	./container hosts 	=> Update Local /etc/hosts

	./container ip [Container ID] 	=> Get Container ip
	./container hostname [Container ID] 	=> Get Container hostname
	./container domain [Container ID] 	=> Get Container domain


------------------


### dockersetlocalhosts

**Depreiciated, please use** `container hosts`

Adds the containers hostname and ip addresses to `etc/hosts` on the host

	dockersetlocalhosts cat => View the /etc/hosts file
	dockersetlocalhosts v => Run in Verbose mode
	dockersetlocalhosts s => Run sillent (will ask for root password if not root)

------------------

